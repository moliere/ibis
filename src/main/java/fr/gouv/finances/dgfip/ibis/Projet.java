package fr.gouv.finances.dgfip.ibis;

import java.util.Date;

public class Projet {

    private Integer idProjet;
    private String refProjet;
    private String libProjet;
    private String descProjet;
    private String versionProjet;
    private Date dateFinProjet;
    private Boolean actif_Projet;

    public Projet() {
    }

    public Projet(Integer idProjet, String refProjet, String libProjet,
	    String descProjet, String versionProjet) {
	super();
	this.idProjet = idProjet;
	this.refProjet = refProjet;
	this.libProjet = libProjet;
	this.descProjet = descProjet;
	this.versionProjet = versionProjet;
    }

    @Override
    public String toString() {
	return "##############################################################\n"
		+ "[" + refProjet + "]\s" + libProjet + "\n\n" + descProjet
		+ "\n\nversion = " + versionProjet + "\n"
		+ "##############################################################\n";
    }

    public Integer getIdProjet() {
	return idProjet;
    }

    public void setIdProjet(Integer idProjet) {
	this.idProjet = idProjet;
    }

    public String getRefProjet() {
	return refProjet;
    }

    public void setRefProjet(String refProjet) {
	this.refProjet = refProjet;
    }

    public String getLibProjet() {
	return libProjet;
    }

    public void setLibProjet(String libProjet) {
	this.libProjet = libProjet;
    }

    public String getDescProjet() {
	return descProjet;
    }

    public void setDescProjet(String descProjet) {
	this.descProjet = descProjet;
    }

    public String getVersionProjet() {
	return versionProjet;
    }

    public void setVersionProjet(String versionProjet) {
	this.versionProjet = versionProjet;
    }

    public Date getDateFinProjet() {
	return dateFinProjet;
    }

    public void setDateFinProjet(Date dateFinProjet) {
	this.dateFinProjet = dateFinProjet;
    }

    public Boolean getActif_Projet() {
	return actif_Projet;
    }

    public void setActif_Projet(Boolean actif_Projet) {
	this.actif_Projet = actif_Projet;
    }

}
